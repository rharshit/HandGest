#an attempt by Shreyansh Murarka to detect hand gestures

import cv2
import numpy as np
import math
import time

#open webcam feed as Camera Object
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH,1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT,720)
cap.set(cv2.CAP_PROP_CONTRAST,0.1)

#initial calibration
flag=False
no_frame=0
pts=[(810,495),(840,390),(890,220),(1030,430),(920,560)]
w=10
h=10
mincr=255
maxcr=0
mincb=255
maxcb=0
low=255
high=0
while(True):
	ret,image=cap.read()
	image=cv2.flip(image,1)
	orig=cv2.blur(image,(3,3))
	image=cv2.blur(image,(10,10))
	im=image.copy()
	for (x,y) in pts:
		cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),2)
	cv2.imshow('feed',image)
	if(cv2.waitKey(1)==97):
		flag=True
	if flag:
		no_frame=no_frame+1
		gray=cv2.cvtColor(orig,cv2.COLOR_BGR2GRAY)
		ycc=cv2.cvtColor(im,cv2.COLOR_BGR2YCrCb)
		for (x,y) in pts:
			for c in range(x,x+w):
				for r in range(y,y+h):
					if(ycc[r][c][1]>maxcr):
						maxcr=ycc[r][c][1]
					if(ycc[r][c][1]<mincr):
						mincr=ycc[r][c][1]
					if(ycc[r][c][2]>maxcb):
						maxcb=ycc[r][c][2]
					if(ycc[r][c][2]<mincb):
						mincb=ycc[r][c][2]
					if(gray[r][c]>high):
						high=gray[r][c]
					if(gray[r][c]<low):
						low=gray[r][c]

	if no_frame==100:
		break

cv2.destroyAllWindows()

#skin color based thresholding
min=np.array([0,mincr,mincb],np.uint8)
max=np.array([255,maxcr,maxcb],np.uint8)
struct=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
face_cascade=cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#starting main loop, press escape to end program
while cv2.waitKey(1)!=27:
	ret,image=cap.read()
	image=cv2.flip(image,1)
	#cv2.imshow('Original',image)

	OriginalImage=cv2.blur(image,(3,3))
	NoFilterImage=image.copy()

	image=cv2.blur(image,(10,10))
	#cv2.imshow('blur',image)
	gray=cv2.cvtColor(OriginalImage,cv2.COLOR_BGR2GRAY)
	ycc=cv2.cvtColor(image,cv2.COLOR_BGR2YCrCb)
	filter=cv2.inRange(ycc,min,max)
	# faces = face_cascade.detectMultiScale(gray)
	# #for (x,y,w,h) in faces:
	# #	for i in range(x,x+w):
	# #		for j in range(y,y+h):
	# 			filter[j][i]=0
	# 			gray[j][i]=0
	# im=NoFilterImage.copy()
	# for i in range(720):
	# 	for j in range(1280):
	# 		if filter[i][j]==0:
	# 			for k in [0,1,2]:
	# 				im[i][j][k]=0

	# gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

	# filter=cv2.erode(filter,struct)
	# filter=cv2.dilate(filter,struct)
	im=NoFilterImage.copy()
	can=cv2.Canny(gray,low,high)
	#_,contours,_ = cv2.findContours(can,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
	cv2.imshow('filtered',filter)
	# for (i,c) in enumerate(contours):
	# 	area=cv2.contourArea(c)
	# 	if area>1000:
	# 		cv2.drawContours(im,contours,i,(255,0,0))
	# cv2.drawContours(im,contours,-1,(255,0,0))
	# for (i,c) in enumerate(contours):
	# 	hull=cv2.convexHull(c,False)
	# 	cv2.drawContours(im,hull,-1,(0,0,255))

	#cv2.imshow('Canny',im)
#destroying allthe windows
cv2.destroyAllWindows()